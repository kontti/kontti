package io.kontti.agent.api.model;

public enum ContainerStatus {
    CREATED("created"),
    RESTARTING("restarting"),
    RUNNING("running"),
    PAUSED("paused"),
    EXITED("exited");

    private String containerStatus;

    ContainerStatus(String status){
        this.containerStatus = status;
    }
 
    public String getContainerStatus() {
        return containerStatus;
    }
}
