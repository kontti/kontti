package io.kontti.agent.services;

import java.util.Arrays;
import java.util.List;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.ListContainersCmd;
import com.github.dockerjava.api.command.ListImagesCmd;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.zerodep.ZerodepDockerHttpClient;

import io.kontti.agent.api.model.ContainerStatus;
import jakarta.inject.Singleton;

@Singleton
public class ContainerService {

    private DockerClient dockerClient;

    public DockerClient getDockerClient() {
        if (dockerClient == null) {
            //DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder().build();
            DockerClientConfig custom = DefaultDockerClientConfig.createDefaultConfigBuilder()
            .withDockerHost("unix:///var/run/docker.sock")
            .withDockerTlsVerify(false)
            .withDockerCertPath("/home/mbuchner/.docker")
            .withRegistryUsername(null)
            .withRegistryPassword(null)
            .withRegistryEmail(null)
            .withRegistryUrl(null)
            .build();

            ZerodepDockerHttpClient client = new ZerodepDockerHttpClient.Builder()
                        .dockerHost(custom.getDockerHost())
                        .sslConfig(custom.getSSLConfig())
                        .build();
            
            dockerClient = DockerClientImpl.getInstance(custom, client);
        }
        return dockerClient;
    }

    public List<Container> listContainers(ContainerStatus containerStatus) {
        ListContainersCmd lcc = getDockerClient().listContainersCmd()
                .withShowSize(true)
                .withShowAll(true);
        if (containerStatus != null) {
            lcc.withStatusFilter(Arrays.asList(containerStatus.getContainerStatus()));
        }
        return lcc.exec();
    }

    public List<Image> listImages(boolean excludeDangling) {
        ListImagesCmd lic = getDockerClient().listImagesCmd()
        .withDanglingFilter(excludeDangling)        
        .withShowAll(true);
        return lic.exec();
    }

}
