package io.kontti.agent.api.resource;

import java.util.List;

import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;

import io.kontti.agent.services.ContainerService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/container")
public class ContainerResource {

    @Inject
    ContainerService containerService;

    @GET
    @Path("images")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Container> listImages() {
        return containerService.listContainers(null);
    }

    @GET
    @Path("containers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Image> listContainers() {
        return containerService.listImages(false);
    }
}
